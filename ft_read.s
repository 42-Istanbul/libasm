%ifdef macOS
	global	_ft_read

	extern	___error
%else
	global	ft_read

	extern	__errno_location
%endif

	section	.text

	; 	INPUT
	; rdi = int fd
	; rsi = void *buf
	; rdx = size_t count

	; 	OUTPUT
	; rax = ssize_t length of read input

%ifdef macOS
_ft_read:					; macOS version
	mov		rax, 0x2000003	; read syscall number
	syscall
	jc		.error
	ret
%else
ft_read:					; linux version
	mov	rax, 0				; read syscall number
	syscall
	cmp		rax, 0
	jl		.error
	ret
%endif

; sets errno and returns -1 on exit
.error:
	push	rax
%ifdef macOS
	call	___error		; call errno function
%else
	call	__errno_location wrt ..plt
%endif
	pop		rdi
%ifndef macOS
	neg		rdi
%endif
	mov		[rax], rdi		; rax = errno, *rax = error value
	mov		rax, -1			; return -1 on exit
	ret
