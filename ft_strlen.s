%ifdef macOS
	global	_ft_strlen
%else
	global	ft_strlen
%endif

	section	.text

	; 	INPUT
	; rdi = const char *s

	; 	OUTPUT
	; rax = length of string

%ifdef macOS
_ft_strlen:
%else
ft_strlen
%endif

	mov	rax, rdi		; rax = rdi = s

.loop:
	cmp	byte [rax], 0	; if (*rax == '\0')
	jz	.return			; then go to return else
	inc	rax				; rax++;
	jmp .loop

.return:
	sub	rax, rdi		; rax = rdi - rax = length of string
	ret
