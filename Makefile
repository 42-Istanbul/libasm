SRCS = ft_strlen.s \
		ft_strcpy.s \
		ft_strcmp.s \
		ft_write.s \
		ft_read.s \
		ft_strdup.s
OBJS = $(SRCS:.s=.o)
SRCS_BONUS = ft_atoi_base_bonus.s \
				ft_list_push_front_bonus.s \
				ft_list_size_bonus.s \
				ft_list_sort_bonus.s \
				ft_list_remove_if_bonus.s
OBJS_BONUS = $(SRCS_BONUS:.s=.o)

NAME = libasm.a
TEST_SRCS = main.c
TEST_NAME = test

UNAME = $(shell uname)
ASM = nasm
ifeq ($(UNAME), Linux)
	ASM_FLAGS = -f elf64 -g -F dwarf
else
	ASM_FLAGS = -f macho64 -D macOS -g -F dwarf
endif

CC = gcc
CFLAGS = -Wall -Wextra -Werror -g
ARFLAGS = -rcs
RM = rm -rf

all: $(NAME)

$(NAME): $(OBJS)
	ar $(ARFLAGS) $(NAME) $(OBJS)

%.o: %.s
	$(ASM) $(ASM_FLAGS) $<

bonus: $(OBJS) $(OBJS_BONUS)
	ar $(ARFLAGS) $(NAME) $(OBJS) $(OBJS_BONUS)

test: $(NAME) $(TEST_SRCS)
	$(CC) $(CFLAGS) $(TEST_SRCS) -L. -lasm -o $(TEST_NAME)
	./$(TEST_NAME)

clean:
	$(RM) $(OBJS) $(OBJS_BONUS)

fclean: clean
	$(RM) $(NAME) $(TEST_NAME)

re: fclean all

.PHONY: all clean fclean re test
