%ifdef macOS
	global	_ft_strcpy
%else
	global	ft_strcpy
%endif

	section	.text

	; 	INPUT
	; rdi = char *dest
	; rsi = const char *src

	; 	OUTPUT
	; rax = char *dest

%ifdef macOS
_ft_strcpy:
%else
ft_strcpy:
%endif

	mov		rax, rdi			; rax = rdi = char *dest

.loop:
	cmp		byte [rsi], 0		; if (*rsi == 0)
	jz		.return				; go to return else
	movsb						; *rdi++ = *rsi++
	jmp		.loop

.return:
	mov		byte [rdi], 0		; add '\0' at the end
	ret
