#include "libasm.h"

int main(void)
{
	int	fd = 1;
	char	*str = "Hello, World";

	printf("\nFT_WRITE TEST\n");
	printf("\nwrite    => [%zd]\n", write(fd, str, 13));
	printf("\nft_write => [%zd]\n", ft_write(fd, str, 13));
	fd = -1;
	printf("write    => [%zd]\n", write(fd, str, 13));
	perror("errno");
	printf("ft_write => [%zd]\n", ft_write(fd, str, 13));
	perror("errno");

	int	fd1 = 10;
	char	buf[10];
	printf("\nFT_READ TEST\n");
	printf("read    => [%zd]\n", read(fd1, buf, 10));
	perror("errno");
	printf("ft_read => [%zd]\n", ft_read(fd1, buf, 10));
	perror("errno");

	printf("\nFT_STRLEN TEST\n");
	printf("strlen    => [%s] = [%zu]\n", str, strlen(str));
	printf("ft_strlen => [%s] = [%zu]\n", str, ft_strlen(str));

	char	dst[20];
	char	*src = "some string";
	printf("\nFT_STRCPY TEST\n");
	printf("strcpy    => src = [%s] dst = [%s]\n", src, strcpy(dst, src));
	bzero(dst, sizeof(dst));
	printf("ft_strcpy => src = [%s] dst = [%s]\n", src, ft_strcpy(dst, src));

	char	*s1 = "\xffhello";
	char	*s2 = "\xff\xff";
	printf("\nFT_STRCMP TEST\n");
	printf("strcmp    => [%s] %c [%s] [%d]\n", s1, strcmp(s1, s2) != 0 ? (strcmp(s1, s2) > 0 ? '>' : '<') : '=', s2, strcmp(s1, s2));
	printf("ft_strcmp => [%s] %c [%s] [%d]\n", s1, ft_strcmp(s1, s2) != 0 ? (ft_strcmp(s1, s2) > 0 ? '>' : '<') : '=', s2, ft_strcmp(s1, s2));

	char	*s3 = "im a looooooooong string";
	printf("\nFT_STRDUP TEST\n");
	printf("strdup    => [%s] [%s]\n", s3, strdup(s3));
	printf("ft_strdup => [%s] [%s]\n", s3, ft_strdup(s3));
	return (0);
}
