%ifdef macOS
	global	_ft_write

	extern	___error
%else
	global	ft_write

	extern	__errno_location
%endif

	section	.text

	; 	INPUT
	; rdi = int fd
	; rsi = const void *buf
	; rdx = size_t count

	; 	OUTPUT
	; rax = ssize_t length of output buffer

%ifdef macOS
_ft_write:					; macOS version
	mov		rax, 0x2000004	; write syscall number
	syscall					; make syscall and write return value into rax
	jc		.error			; go to error subroutine
	ret						; return to calling function
%else
ft_write:					; linux version
	mov		rax, 1			; write syscall number
	syscall					; make syscall and write return value into rax
	cmp		rax, 0			; check for return value
	jl		.error			; go to error subroutine
	ret						; return to calling function
%endif

; sets errno and returns -1 on exit
.error:
	push	rax
%ifdef macOS
	call	___error		; call errno function
%else
	call	__errno_location wrt ..plt
%endif
	pop		rdi
%ifndef macOS
	neg		rdi
%endif
	mov		[rax], rdi		; rax = pointer to int, *rax = error value
	mov		rax, -1			; return -1 on exit
	ret
