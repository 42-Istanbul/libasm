%ifdef macOS
	global	_ft_strdup

	extern	_ft_strlen
	extern	_ft_strcpy
	extern	_malloc
	extern	___error
%else
	global	ft_strdup

	extern	ft_strlen
	extern	ft_strcpy
	extern	malloc
	extern	__errno_location
%endif

	section	.text

	; 	INPUT
	; rdi = const char *s

	; 	OUTPUT
	; rax = char * to malloc'd string

%ifdef macOS
_ft_strdup:

	call	_ft_strlen	; call ft_strlen to get length of s
%else
ft_strdup:

	call	ft_strlen	; call ft_strlen to get length of s
%endif

.allocate_space:
	push	rdi				; save rdi to stack
	mov		rdi, rax		; pass length to rdi to be parameter
%ifdef macOS
	call	_malloc			; call malloc and store address in rax
%else
	call	malloc wrt ..plt
%endif
	cmp		rax, 0			; check if address is valid
	jl		.error
	mov		rdi, rax		; store address in rdi
	pop		rsi				; return rdi value from stack in rsi

; call strcpy to copy from rsi to rdi
%ifdef macOS
	call	_ft_strcpy
%else
	call	ft_strcpy
%endif
	ret

; sets errno and returns -1 on exit
.error:
	mov		r8, rax			; rdi = -error value
	neg		r8				; make error value positive
%ifdef macOS
	call	___error		; call errno function
%else
	call	__errno_location wrt ..plt
%endif
	mov		[rax], r8		; rax = pointer to int, *rax = error value
	mov		rax, -1			; return -1 on exit
	ret
