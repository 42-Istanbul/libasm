%ifdef macOS
	global	_ft_strcmp
%else
	global	ft_strcmp
%endif

	section	.text

	; 	INPUT
	; rdi = const char *s1
	; rsi = const char *s2

	;	OUTPUT
	; rax = + if s1 > s2
	;		- if s1 < s2
	;		0 if s1 == s2

%ifdef macOS
_ft_strcmp:
%else
ft_strcmp:
%endif

.loop:
	mov		r8b, byte [rdi]	; r8b = *rdi
	cmp		r8b, byte [rsi]	; check if r8b == r9b a.k.a *rdi == *rsi
	jnz		.return
	cmp		byte [rdi], 0	; check if *rsi == '\0
	jz		.return
	inc		rdi				; rdi++
	inc		rsi				; rsi++
	jmp		.loop

.return:
	movzx	r8, byte [rdi]	; copy byte from rdi and zero extend in r8
	movzx	r9, byte [rsi]
	sub		r8, r9
	mov		rax, r8
	ret
